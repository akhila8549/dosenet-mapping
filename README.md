# DoseNet Mapping

This repository maintians all software related to the DoseNet Mapping project. This software is for interfacing Raspberry Pi computers with the suit of environmnetal and radiation sensors used by the DoseNet project as part of our portable sensor systems.

This code will only run on a Raspberry Pi with a subset of hand-picked sensors, primarily provided by Adafruit, and uses the Adafruit Circuit-Python packages as the drivers for each of these sensors. The radiation sensors used have special data acquisition code. Most of the code in this repository is specif to the user interface developed for runing a graphical display of data as it is actively read from our sensors.